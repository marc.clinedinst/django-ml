from django.template.defaultfilters import slugify
from factory import Faker, LazyAttribute
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyChoice, FuzzyText

from .models import Cheese


class CheeseFactory(DjangoModelFactory):
    country_of_origin = Faker("country_code")
    description = Faker("paragraph", nb_sentences=3, variable_nb_sentences=True)
    firmness = FuzzyChoice(x[0] for x in Cheese.Firmness.choices)
    name = FuzzyText()
    slug = LazyAttribute(lambda obj: slugify(obj.name))

    class Meta:
        model = Cheese
