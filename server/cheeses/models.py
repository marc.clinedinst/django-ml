from autoslug import AutoSlugField
from django.db import models
from django.urls import reverse
from django_countries.fields import CountryField
from model_utils.models import TimeStampedModel


class Cheese(TimeStampedModel):
    class Firmness(models.TextChoices):
        HARD = "hard", "Hard"
        SEMI_HARD = "semi-hard", "Semi-Hard"
        SEMI_SOFT = "semi-soft", "Semi-Soft"
        SOFT = "soft", "Soft"
        UNSPECIFIED = "unspecified", "Unspecified"

    country_of_origin = CountryField("Country of Origin", blank=True)
    description = models.TextField("Description", blank=True)
    firmness = models.CharField(
        "Firmness",
        choices=Firmness.choices,
        default=Firmness.UNSPECIFIED,
        max_length=20,
    )
    name = models.CharField("Name of Cheese", max_length=255)
    slug = AutoSlugField(
        "Cheese Address", always_update=False, populate_from="name", unique=True
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        """Return absolute URL to the Cheese Detail page."""
        return reverse("cheeses:detail", kwargs={"slug": self.slug})
