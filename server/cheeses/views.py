from django.views.generic import CreateView, DetailView, ListView

from .models import Cheese


class CheeseCreateView(CreateView):
    fields = ["name", "description", "firmness", "country_of_origin"]
    model = Cheese


class CheeseDetailView(DetailView):
    model = Cheese


class CheeseListView(ListView):
    model = Cheese
