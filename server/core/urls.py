from django.urls import path

from .views import HomePageView, csv_view, pdf_view, MyView

urlpatterns = [
    path("", HomePageView.as_view(), name="home_page"),
    path("csv-view/", csv_view, name="csv_view"),
    path("pdf-view/", pdf_view, name="some_view"),
    path("my-view/", MyView.as_view(), name="my_view"),
]
