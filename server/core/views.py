import io
import csv

from django.http import FileResponse, HttpResponse
from django.views.generic import TemplateView, View
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas

from movies.models import Movie


class HomePageView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["my_statement"] = "Nice to see you!"

        return context

    def say_bye(self):
        return "Goodbye!"


def csv_view(request):
    response = HttpResponse(
        content_type="text/csv",
        headers={"Content-Disposition": 'attachment; filename="somefilename.csv"'},
    )
    writer = csv.writer(response)

    writer.writerow(["Genre", "Title", "Movie"])

    for movie in Movie.objects.all():
        writer.writerow([movie.genre, movie.title, movie.year])

    return response


def pdf_view(request):
    buffer = io.BytesIO()
    c = canvas.Canvas(buffer, pagesize=A4)

    c.setAuthor("Marcus Farcus")
    c.translate(inch, inch)
    c.setFont("Helvetica", 14)
    c.setStrokeColorRGB(0.2, 0.5, 0.3)
    c.setFillColorRGB(1, 0, 1)
    c.line(0, 0, 0, 1.7 * inch)
    c.line(0, 0, inch, 0)
    c.rect(0.2 * inch, 0.2 * inch, 1 * inch, 1.5 * inch, fill=1)
    c.rotate(90)
    c.setFillColorRGB(0, 0, 0.77)
    c.drawString(0.3 * inch, -inch, "Hello, World!")

    c.showPage()
    c.save()

    c.translate(inch, inch)
    c.setFont("Helvetica", 14)
    c.setStrokeColorRGB(0.2, 0.5, 0.3)
    c.setFillColorRGB(1, 0, 1)
    c.line(0, 0, 0, 1.7 * inch)
    c.line(0, 0, inch, 0)
    c.rect(0.2 * inch, 0.2 * inch, 1 * inch, 1.5 * inch, fill=1)
    c.rotate(90)
    c.setFillColorRGB(0, 0, 0.77)
    c.drawString(0.3 * inch, -inch, "Hello, World!")

    buffer.seek(0)

    return FileResponse(buffer, as_attachment=True, filename="hello.pdf")


class MyView(View):
    def delete(self, request, *args, **kwargs):
        return HttpResponse("Response to DELETE request.")

    def get(self, request, *args, **kwargs):
        return HttpResponse("Response to GET request.")

    def post(self, request, *args, **kwargs):
        return HttpResponse("Response to POST request.")
