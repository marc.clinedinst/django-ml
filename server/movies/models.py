from django.db import models


class Movie(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    genre = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    year = models.CharField(max_length=4)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
