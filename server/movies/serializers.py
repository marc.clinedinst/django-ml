from rest_framework import serializers

from .models import Movie


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Movie
        read_only_fields = ("id", "created_date", "updated_date")
