from django.urls import path

from .views import MovieDetail, MovieList, Foo


urlpatterns = [
    path("api/movies/", MovieList.as_view()),
    path("api/movies/<int:pk>/", MovieDetail.as_view()),
    path("api/foo/", Foo.as_view()),
]
