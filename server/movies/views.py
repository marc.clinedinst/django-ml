from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Movie
from .serializers import MovieSerializer


class MovieDetail(APIView):
    def delete(self, request, pk, format=None):
        movie = get_object_or_404(Movie, pk=pk)
        movie.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def get(self, request, pk, format=None):
        movie = get_object_or_404(Movie, pk=pk)
        serializer = MovieSerializer(movie)

        return Response(serializer.data)

    def put(self, request, pk, format=None):
        movie = get_object_or_404(Movie, pk=pk)
        serializer = MovieSerializer(movie, data=request.data)

        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MovieList(APIView):
    def get(self, request, format=None):
        movies = Movie.objects.all()
        serializer = MovieSerializer(movies, many=True)

        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = MovieSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Foo(APIView):
    def get(self, request, format=None):
        return Response({"number": False})

    def post(self, request, format=None):
        return Response(request.data)
