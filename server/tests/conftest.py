import pytest

from movies.models import Movie


@pytest.fixture
def add_movie():
    def _add_movie(genre, title, year):
        return Movie.objects.create(genre=genre, title=title, year=year)

    return _add_movie
