import pytest

from cheeses.factories import CheeseFactory


@pytest.mark.django_db
class TestCheeseModel:
    def test_string_representation(self):
        cheese = CheeseFactory()

        assert cheese.__str__() == cheese.name
        assert str(cheese) == cheese.name
