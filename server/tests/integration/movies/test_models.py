import pytest

from movies.models import Movie


class TestModels:
    @pytest.mark.django_db
    def test_movie_model(self):
        movie = Movie(genre="comedy", title="Raising Arizona", year="1987")
        movie.save()

        assert movie.created_date
        assert movie.genre == "comedy"
        assert movie.title == "Raising Arizona"
        assert movie.year == "1987"
        assert movie.updated_date
        assert str(movie) == movie.title
