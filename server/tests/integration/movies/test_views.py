import pytest

from movies.models import Movie


class TestAddMovie:
    @pytest.mark.django_db
    def test_add_movie(self, client):
        assert len(Movie.objects.all()) == 0

        response = client.post(
            "/api/movies/",
            {"genre": "comedy", "title": "The Big Lebowski", "year": "1998"},
            content_type="application/json",
        )

        assert response.status_code == 201
        assert response.data["title"] == "The Big Lebowski"
        assert len(Movie.objects.all()) == 1

    @pytest.mark.django_db
    def test_add_movie_invalid_json(self, client):
        assert len(Movie.objects.all()) == 0

        response = client.post("/api/movies/", {}, content_type="application/json")

        assert response.status_code == 400
        assert len(Movie.objects.all()) == 0

    @pytest.mark.django_db
    def test_add_movie_invalid_json_keys(self, client):
        assert len(Movie.objects.all()) == 0

        response = client.post(
            "/api/movies/",
            {"genre": "comedy", "title": "The Big Lebowski"},
            content_type="application/json",
        )

        assert response.status_code == 400
        assert len(Movie.objects.all()) == 0


class TestGetAllMovies:
    @pytest.mark.django_db
    def test_get_all_movies(self, add_movie, client):
        movie_one = add_movie("comedy", "The Big Lebowski", "1998")
        movie_two = add_movie("thriller", "No Country for Old Men", "2007")
        response = client.get("/api/movies/")

        assert response.status_code == 200
        assert response.data[0]["title"] == movie_one.title
        assert response.data[1]["title"] == movie_two.title


class TestGetSingleMovie:
    @pytest.mark.django_db
    def test_get_single_movie(self, add_movie, client):
        movie = add_movie("comedy", "The Big Lebowski", "1998")
        response = client.get(f"/api/movies/{movie.id}/")

        assert response.status_code == 200
        assert response.data["title"] == "The Big Lebowski"

    def test_get_single_movie_incorrect_id(self, client):
        response = client.get("/api/movies/foo/")

        assert response.status_code == 404


class TestRemoveMovie:
    @pytest.mark.django_db
    def test_remove_movie(self, add_movie, client):
        movie = add_movie(genre="comedy", title="The Big Lebowski", year="1998")
        response = client.get(f"/api/movies/{movie.id}/")

        assert response.data["title"] == "The Big Lebowski"
        assert response.status_code == 200

        response_two = client.delete(f"/api/movies/{movie.id}/")

        assert response_two.status_code == 204

        response_three = client.get("/api/movies/")

        assert len(response_three.data) == 0
        assert response_three.status_code == 200

    @pytest.mark.django_db
    def test_remove_movie_incorrect_id(self, client):
        response = client.delete("/api/movies/99/")

        assert response.status_code == 404


class TestUpdateMovie:
    @pytest.mark.django_db
    def test_update_movie(self, add_movie, client):
        movie = add_movie(genre="comedy", title="The Big Lebowski", year="1998")

        response = client.put(
            f"/api/movies/{movie.id}/",
            {"genre": "comedy", "title": "The Big Lebowski", "year": "1997"},
            content_type="application/json",
        )

        assert response.status_code == 200
        assert response.data["title"] == "The Big Lebowski"
        assert response.data["year"] == "1997"

        response_two = client.get(f"/api/movies/{movie.id}/")

        assert response_two.status_code == 200
        assert response_two.data["title"] == "The Big Lebowski"
        assert response.data["year"] == "1997"

    @pytest.mark.django_db
    def test_update_movie_incorrect_id(self, add_movie, client):
        movie = add_movie(genre="comedy", title="The Big Lebowski", year="1998")

        response = client.put(
            f"/api/movies/{movie.id + 1}/",
            {"genre": "comedy", "title": "The Big Lebowski", "year": "1997"},
            content_type="application/json",
        )

        assert response.status_code == 404
        assert movie.year == "1998"

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "payload, status_code",
        [[{}, 400], [{"genre": "comedy", "title": "The Big Lebowski"}, 400]],
    )
    def test_update_movie_invalid_json(self, add_movie, client, payload, status_code):
        movie = add_movie(genre="comedy", title="The Big Lebowski", year="1998")

        response = client.put(
            f"/api/movies/{movie.id}/", payload, content_type="application/json"
        )

        assert response.status_code == status_code
