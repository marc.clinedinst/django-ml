import json

from django.urls import reverse


def test_hello_world():
    assert "hello world" == "hello world"
    assert "foo" != "bar"


def test_ping(client):
    response = client.get(reverse("ping"))
    content = json.loads(response.content)

    assert response.status_code == 200
    assert content["ping"] == "pong!"
