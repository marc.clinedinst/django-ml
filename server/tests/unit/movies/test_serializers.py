from movies.serializers import MovieSerializer


class TestSerializers:
    def test_valid_movie_serializer(self):
        valid_serializer_data = {
            "genre": "comedy",
            "title": "Raising Arizona",
            "year": "1987",
        }
        serializer = MovieSerializer(data=valid_serializer_data)

        assert serializer.is_valid()
        assert serializer.validated_data == valid_serializer_data
        assert serializer.data == valid_serializer_data
        assert serializer.errors == {}

    def test_invalid_movie_serializer(self):
        invalid_serializer_data = {"genre": "comedy", "title": "Raising Arizona"}
        serializer = MovieSerializer(data=invalid_serializer_data)

        assert not serializer.is_valid()
        assert serializer.validated_data == {}
        assert serializer.data == invalid_serializer_data
        assert serializer.errors == {"year": ["This field is required."]}
