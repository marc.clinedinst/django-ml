import pytest

from django.http import Http404
from movies import views
from movies.models import Movie
from movies.serializers import MovieSerializer


class TestAddMovie:
    def test_add_movie(self, client, monkeypatch):
        payload = {"genre": "comedy", "title": "The Big Lebowski", "year": "1998"}

        def mock_create(self, payload):
            return "The Big Lebowski"

        monkeypatch.setattr(MovieSerializer, "create", mock_create)
        monkeypatch.setattr(MovieSerializer, "data", payload)

        response = client.post("/api/movies/", payload, content_type="application/json")

        assert response.status_code == 201
        assert response.data["title"] == "The Big Lebowski"

    def test_add_movie_invalid_json(self, client):
        response = client.post("/api/movies/", {}, content_type="application/json")

        assert response.status_code == 400

    def test_add_movie_invalid_json_keys(self, client):
        response = client.post(
            "/api/movies/",
            {"genre": "comedy", "title": "The Big Lebowski"},
            content_type="application/json",
        )

        assert response.status_code == 400


class TestGetAllMovies:
    def test_get_all_movies(self, client, monkeypatch):
        payload = [
            {"genre": "comedy", "title": "The Big Lebowski", "year": "1998"},
            {"genre": "thriller", "title": "No Country for Old Men", "year": "2007"},
        ]

        def mock_get_all_movies():
            return payload

        monkeypatch.setattr(Movie.objects, "all", mock_get_all_movies)
        monkeypatch.setattr(MovieSerializer, "data", payload)

        response = client.get("/api/movies/")

        assert response.status_code == 200
        assert response.data[0]["title"] == payload[0]["title"]
        assert response.data[1]["title"] == payload[1]["title"]


class TestGetSingleMovie:
    def test_get_single_movie(self, client, monkeypatch):
        payload = {"genre": "comedy", "title": "The Big Lebowski", "year": "1998"}

        def mock_get_object_or_404(self, pk):
            return 1

        monkeypatch.setattr(views, "get_object_or_404", mock_get_object_or_404)
        monkeypatch.setattr(MovieSerializer, "data", payload)

        response = client.get("/api/movies/1/")

        assert response.status_code == 200
        assert response.data["title"] == "The Big Lebowski"

    def test_get_single_movie_incorrect_id(self, client):
        response = client.get("/api/movies/foo/")

        assert response.status_code == 404


class TestRemoveMovie:
    def test_remove_movie(self, client, monkeypatch):
        def mock_get_object_or_404(self, pk):
            class Movie:
                def delete():
                    pass

            return Movie

        monkeypatch.setattr(views, "get_object_or_404", mock_get_object_or_404)

        response = client.delete("/api/movies/1/")

        assert response.status_code == 204

    def test_remove_movie_incorrect_id(self, client, monkeypatch):
        def mock_get_object_or_404(self, pk):
            raise Http404

        monkeypatch.setattr(views, "get_object_or_404", mock_get_object_or_404)

        response = client.delete("/api/movies/99/")

        assert response.status_code == 404


class TestUpdateMovie:
    def test_update_movie(self, client, monkeypatch):
        payload = {"genre": "comedy", "title": "The Big Lebowski", "year": "1997"}

        def mock_get_object_or_404(self, pk):
            return 1

        def mock_update_object(self, movie_object, data):
            return payload

        monkeypatch.setattr(views, "get_object_or_404", mock_get_object_or_404)
        monkeypatch.setattr(MovieSerializer, "update", mock_update_object)

        response = client.put(
            "/api/movies/1/", payload, content_type="application/json"
        )

        assert response.status_code == 200
        assert response.data["title"] == payload["title"]
        assert response.data["year"] == payload["year"]

    def test_update_movie_incorrect_id(self, client, monkeypatch):
        def mock_get_object_or_404(self, pk):
            raise Http404

        monkeypatch.setattr(views, "get_object_or_404", mock_get_object_or_404)

        response = client.put("/api/movies/99/")

        assert response.status_code == 404

    @pytest.mark.parametrize(
        "payload, status_code",
        [[{}, 400], [{"genre": "comedy", "title": "The Big Lebowski"}, 400]],
    )
    def test_update_movie_invalid_json(self, client, monkeypatch, payload, status_code):
        def mock_get_object_or_404(self, pk):
            return 1

        monkeypatch.setattr(views, "get_object_or_404", mock_get_object_or_404)

        response = client.put(
            "/api/movies/1/", payload, content_type="application/json"
        )

        assert response.status_code == status_code
