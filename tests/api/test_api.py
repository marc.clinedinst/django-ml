import logging
import os
import pytest
import requests

HEROKU_SERVER_PREVIEW_APP_NAME = os.environ.get('HEROKU_SERVER_PREVIEW_APP_NAME')


@pytest.mark.server
class TestAPI:
    def test_ping(self):
        response = requests.get(f'https://{HEROKU_SERVER_PREVIEW_APP_NAME}.herokuapp.com/ping/')
        logging.info(response.json())
