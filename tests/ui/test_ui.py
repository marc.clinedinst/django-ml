import logging
import os
import pytest
import time

from selenium import webdriver

HEROKU_CLIENT_PREVIEW_APP_NAME = os.environ.get('HEROKU_CLIENT_PREVIEW_APP_NAME')
HEROKU_SERVER_PREVIEW_APP_NAME = os.environ.get('HEROKU_CLIENT_PREVIEW_APP_NAME')


class TestUI:
    @pytest.fixture(scope='session')
    def browser(self):
        caps = {'browserName': os.getenv('BROWSER', 'chrome')}
        return webdriver.Remote(
            command_executor='http://selenium__standalone-chrome:4444/wd/hub',
            desired_capabilities=caps
        )

    @pytest.mark.client
    def test_my_client(self, browser):
        logging.info("just testing my site")
        browser.get(f'https://{HEROKU_CLIENT_PREVIEW_APP_NAME}.herokuapp.com/')
        time.sleep(3)

        header = browser.find_element_by_css_selector('h1')
        logging.info(header.text)

    @pytest.mark.server
    def test_my_server(self, browser):
        logging.info("just testing my site")
        browser.get(f'https://{HEROKU_SERVER_PREVIEW_APP_NAME}.herokuapp.com/')
        time.sleep(3)

        header = browser.find_element_by_css_selector('h1')
        logging.info(header.text)
